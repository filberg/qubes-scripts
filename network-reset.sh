#!/bin/sh

# This file is part of filberg's qubes-bashed-potatoes
# 
#  Copyright (c) 2020 Filippo Bergamo <fil@filberg.eu>
# 
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software Foundation,
# Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301  USA


. "$(dirname $0)/lib-network-reset.sh"

vm="sys-net"
sleepsecs=2

helptsr="
    USAGE: $0

    Resets the PCI network interface in the target VM.

    Target VM defaults to \"$vm\", and may be changed via:
       export FILBERG_NETRESET_VM=<target_vm_name>

    Sleep time between commands defaults to $sleepsecs and may be adjusted via:
       export FILBERG_NETRESET_SLEEPSEC=<time_in_seconds> 
"

[ $# -gt 0 ] &&
    {
	echo "$helptsr"

	case "$1" in
	    -h|--help)
		exit 0;;
	    *)
		exit 1;;
	esac
    }

[ -z "$FILBERG_NETRESET_VM" ] &&
    export FILBERG_NETRESET_VM="$vm"

[ -z "$FILBERG_NETRESET_SLEEPSEC" ] &&
    export FILBERG_NETRESET_SLEEPSEC="$sleepsecs"

netreset_read_env

netreset_check_env ||
    exit $?

network_reset ||
    {
	ec=$?
	_log "* Network Reset FAILED!"
	exit $ec
    }

