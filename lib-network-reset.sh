#!/bin/sh

# This file is part of filberg's qubes-bashed-potatoes
# 
#  Copyright (c) 2020 Filippo Bergamo <fil@filberg.eu>
# 
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software Foundation,
# Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301  USA


. "$(dirname $0)/lib.sh"

target_vm="sys-net"
sec_wait=1
sec_wait_svc=5
pci_ctrl_root="/sys/devices/pci0000:00"

needed_envars="\
FILBERG_NETRESET_DEVNAME \
FILBERG_NETRESET_MODULES"


# $1 = message
_log()
{
    echo "$1"
    notify-send "$1"
}

netreset_check_env()
{
    check_env_vars $needed_envars
}

# $1 = list of modules (whitespace-separated)
modules_remove()
{
    for m in $1
    do
	qvm-run "$target_vm" "sudo rmmod $m" ||
	    {
		_log "FAILED to remove module $m"
		return 1
	    }

	_log "Removed module $m."
	
    done

    _log "OK!"
}


# $1 = device name
pci_get_devnum()
{
    qvm-run --pass-io "$target_vm" \
	    lspci -vv | grep "$1" | cut -d ' ' -f 1
}

# $1 = device name
pci_dev_remove()
{
    num=$(pci_get_devnum "$1")

    qvm-run "$target_vm" \
	    "echo 1 | sudo tee \"$pci_ctrl_root/0000:$num/remove\"" ||
	{
	    _log "FAILED to remove PCI device \"$1\""
	    return 1
	}

    _log "Removed PCI device \"$1\""
}

pci_rescan()
{
    qvm-run "$target_vm" \
	    "echo 1 | sudo tee /sys/bus/pci/rescan"
}

# $1 = service name
# $2 = start | stop
_svc()
{
    qvm-run --pass-io "$target_vm" \
	    "sudo systemctl $2 \"$1\"" ||
	{
	    _log "FAILED to $2 $1"
	    return 1
	}
}

network_reset()
{
    _log "Stopping network services..."

    _svc NetworkManager stop ||
	return $?

    sleep $((sec_wait + sec_wait_svc))

    _svc networking stop ||
	return $?

    sleep $((sec_wait + sec_wait_svc))

    modules_remove "$FILBERG_NETRESET_MODULES"
    sleep $sec_wait

    pci_dev_remove "$FILBERG_NETRESET_DEVNAME"
    sleep $sec_wait

    _log "Now rescanning pci devices..."

    pci_rescan ||
	return $?

    sleep $sec_wait

    _svc networking start ||
	return $?

    sleep $sec_wait

    _svc NetworkManager start
}

netreset_read_env()
{
    [ -n "$FILBERG_NETRESET_VM" ] &&
	target_vm="$FILBERG_NETRESET_VM"

    [ -n "$FILBERG_NETRESET_SLEEPSEC" ] &&
	sec_wait="$FILBERG_NETRESET_SLEEPSEC"
}
