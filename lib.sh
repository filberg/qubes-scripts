#!/bin/sh

# This file is part of filberg's qubes-bashed-potatoes
# 
#  Copyright (c) 2020 Filippo Bergamo <fil@filberg.eu>
# 
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software Foundation,
# Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301  USA

die()
{
    echo "$1"
    [ $# -gt 1 ] &&
	exit $2

    exit 1
}

printerr()
{
    echo "$1" >&2
}

# $1 = prompt text
# $2 = NON-default choices (pipe-separated list)
# Retruns 0 if the default choice was confirmed
# Returns 1 if the NON-default choiche was made
_prompt_default()
{
    print "$1 "
    read ans
    case $ans in
	$2)
	    return 1
	    ;;
	*)
	    return 0
	    ;;
    esac

}

# $1 = prompt text
# Returns 0 if 'Y', 'y' or default was chosed
# Returns 1 if 'N' or 'n' was chosed
prompt_default_yes()
{
    _prompt "$1 [Y/n]" "N|n"
}

# $1 = prompt text
# Returns 0 if 'Y' or 'y'was chosed
# Returns 1 if 'N', 'n' or default was chosed
prompt_default_no()
{
    _prompt "$1 [N/y]" "Y|y" ||
	return 1

    return 0
}

# $@ = list of environment variables to check
check_env_vars()
{
    [ $# -eq 0 ] &&
	{
	    printerr "check_env_vars: at least 1 argument is required."
	    return 1
	}

    for ev in "$@"
    do
	printenv "$ev" > /dev/null ||
	    {
		printerr "ERROR: \$$ev is not defined!"
		return 1
	    }
    done

    return 0
}

