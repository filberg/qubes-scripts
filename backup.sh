#!/bin/bash

# This file is part of filberg's qubes-bashed-potatoes
# 
#  Copyright (c) 2020 Filippo Bergamo <fil@filberg.eu>
# 
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software Foundation,
# Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301  USA


scriptdir=$(dirname $0)
. "$scriptdir/lib-qvm.sh"
. "$scriptdir/lib-backup.sh"

usg="
    USAGE: $0 [profile]

    If [profile] is not specified, all profiles are run.
"

profiles_file="$HOME/.filberg/scripts/qubes/backup-profiles.conf"

profiles=""
profile_count=0

_is_valid_profile()
{
    for p in $profiles
    do
	[ "$1" = "$p" ] &&
	    return 0
    done

    return 1
}

_handle_arg()
{
    if [ "$1" = "-h" ] || [ "$1" = "--help" ]
    then
	echo "$usg"
	exit 0

    elif _is_valid_profile "$1"
    then
	 target="$1"

    else
	echo "Invalid profile \"$1\""
	exit 128

    fi
}

_read_profiles()
{
    [ -f "$profiles_file" ] ||
	{
	    echo "ERROR: Profiles list not found at $profiles_file"
	    return 64
	}

    while read p
    do
	case "$p" in

	    ''|\#*) # skip empty and comment lines
		;;
	    *) profiles="$profiles $p"
	       profile_count=$((profile_count+1))
	       ;;
	esac
    done < "$profiles_file"

    [ $profile_count -eq 0 ] &&
	{
	    "ERROR: no profile defined in $profiles_file"
	    return 32
	}

    target="$profiles"

    return 0
}

[ $# -gt 1 ] &&
    {
	echo "Too many arguments!"
	echo "$usg"
	exit 1
    }

backup_check_env ||
    exit $?

_read_profiles ||
    exit $?

[ -n "$1" ] &&
    {
	_handle_arg "$1" ||
	    exit $?
    }

okk=
failed=

for p in $target
do
    backup_run "$p" "--yes" ||
	{
	    failed="$failed $p"
	    echo "*** FAILED *** to backup $p"
	    continue
	}

    okk="$okk $p"
    echo "$p backed up successfully"
done

echo ""
echo "------------------------------"
echo "Backup finished"
echo "OK: $okk"
echo "Failed: $failed"
echo "------------------------------"
echo ""
