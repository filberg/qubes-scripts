#!/bin/bash

# This file is part of filberg's qubes-bashed-potatoes
# 
#  Copyright (c) 2020 Filippo Bergamo <fil@filberg.eu>
# 
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software Foundation,
# Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301  USA

. "$(dirname $0)/lib-wm.sh"

winvm="win7"
rdpscript=$FILBERG_SCRIPT_DIR/winrdp.sh
win_wait_for_start=3
win_wait_for_discon=5
netvm_default="sys-net"
dummy_vm="vault"
shutdown_dummy_after_netvm_start=false

function notify()
{
    notify-send "$1"
    echo "$1"
}

# $1 = qvm-ls --raw-data output line
# $2 = field number
function _get_raw_field()
{
    cut -d '|' -f $2 <<< "$1" |
	cut -d ' ' -f 1
}

function list_vms(){
    qvm-ls --raw-list "$1"
}

function qvm-ls-running(){
    list_vms --running
}

function qvm-ls-all(){
    list_vms --all
}

function qvm-ls-halted(){
    list_vms --halted
}

# $1 = vm name to be checked
# returns 0 (true) if the given vm is running
# return 1 (false) otherwise
function qvm-is-running(){
    for vm in $(qvm-ls-running); do
	if [ "$vm" = "$1" ];then
	    return 0;
	fi
    done
    return 1
}

# $1 = vmname
function qvm-start-with-device(){
    nvm=$1

    if [ -z "$nvm" ]
    then
	echo "The vm name must be specified!"
	return 1
    fi
    
    if qvm-is-running "$nvm" ;then
	echo "$nvm is already running"
	return 0
    fi

    if ! qvm-is-running "$dummy_vm" ;then
	qvm-start "$dummy_vm"
	sleep 2
    fi
    
    qvm-start "$nvm" &&
	$shutdown_dummy_after_netvm_start &&
	qvm-shutdown "$dummy_vm"
}

# $1 = [optional] vmname
function qvm-net-start(){
    nvm=$1
    [ -z "$nvm" ] &&
	nvm="$netvm_default"

    qvm-start-with-device "$nvm"
}

function qvm-win-start(){
    nvm=$(qvm-get-upstream-netvm "$winvm")
    if ! qvm-is-running "$winvm" ;then
	qvm-net-start "$nvm"
	qvm-start "$winvm"
	sleep $win_wait_for_start
    fi

    winid=$(wm_get_win_id_from_title "$winvm")

    [ -n "$winid" ] &&
	xkill -id "$winid"

    sleep $win_wait_for_discon

    $rdpscript "$winvm";
}

# $1 = vmname
function qvm-get-upstream-netvm(){

    firsthop=$(qvm-prefs "$1" netvm)

    [ -z "$firsthop" ] &&
	echo "" &&
	return 0

    nexthop=$(qvm-get-upstream-netvm "$firsthop")

    [ -z "$nexthop" ] &&
	echo "$firsthop" &&
	return 0

    echo "$nexthop"
    return 0    
}    

# $1 = vmname
function qvm-start-with-net(){
    vmname=$1
    qvm-net-start
    qvm-is-running $vmname ||
    qvm-start $vmname
    return $?
}

# $1 = device class ( usb | block)
# $2 = device name regex
function qvm-device-get-backend()
{
    [ -z "$1" ] &&
	return 1
    for be in `qvm-device $1 ls | grep "$2"`
    do
	echo "$be"
	break
    done
}

# $1 = device backend (<vmname>:<partname>)
function qvm-device-backend-get-disk()
{
    be=$(cut -d ':' -f 1 <<< "$1" ) ||
	return $?

    dev=$(cut -d ':' -f 2 <<< "$1" ) ||
	return $?

    disk=$(qvm-run --pass-io $be "lsblk -n -o PKNAME /dev/$dev" \
	       | tr -d '\n') ||
	return $?

    disk=$(tr -d '\n' <<< "$disk") ||
	return $?

    [ -z "$disk" ] &&
	return 1;
    
    echo "$be:$disk"
}

# $1 = device name regex
function qvm-usb-get-backend()
{
    qvm-device-get-backend "usb" "$1"
}

# $1 = device name regex
function qvm-block-get-backend-disk()
{
    devs=$(qvm-device-get-backend "block" "$1") ||
	return $?

    [ -z "$devs" ] &&
	return 1

    for d in $devs
    do
	qvm-device-backend-get-disk "$d" ||
	    return $?
	break
    done
}

# $1 = disk name (grep expr)
# $2 = target vm
qvm-disk-attach()
{
    be=$(qvm-block-get-backend-disk "$1") ||
	return $?

    qvm-block attach "$2" "$be"
}

# $1 = backend
qvm-block-get-frontend()
{
    found=0

    IFS="$IFS=(),"
    
    for p in `qvm-block | grep "$1"`
    do
	[ "$p" = "frontend-dev" ] &&
	    {
		found=1
		continue
	    }

	[ $found -eq 1 ] &&
	    {
		echo $p
		return 0
	    }
    done

    return 1
}

# $1 = device name
# $2 = target vm
# $3 = "attach" | "detach"
function qvm-usb-attach-detach()
{
    usg="usb_device_attach DEVICE_NAME TARGET_VM"

    ( [ -z "$1" ] || [ -z "$2" ] ) &&
	{
	    echo "$usg"
	    return 1
	}

    backend=$(qvm-usb-get-backend "$1") ||
	{
	    echo "FAILED to get USB backend for $1"
	    return 1
	}

    [ -z "$backend" ] &&
	{
	    echo "Got empty USB backend for $1"
	    return 1
	}

    qvm-usb $3 "$2" "$backend" ||
	{
	    echo "FAILED to $3 $1 <--> $2"
	    return 1
	}

    echo "$3ed $1 to $2"
    return 0
}

# $1 = device name
# $2 = target vm
function qvm-usb-attach()
{
    qvm-usb-attach-detach "$1" "$2" "attach"
}

# $1 = device name
# $2 = target vm
function qvm-usb-detach()
{
    qvm-usb-attach-detach "$1" "$2" "detach"
}

# $1 = netvm
# $2 = client vm
function qvm-netvm-assign()
{
    printf "Assigning netvm %s to %s......" "$1" "$2"

    qvm-prefs "$2" netvm "$1" ||
	{
	    echo "FAILED!"
	    return 1
	}

    echo "OK"
    return 0
}

# $1 = netvm
# $2 = client vms (space-separated list)
function qvm-netvm-assign-bulk()
{
    for c in $2
    do
	qvm-netvm-assign "$1" "$c" ||
	    return 1
    done

    return 0
}

# $1 = vmname
# $2 = command
function run-cmd-notify()
{
    notify "Running $2 on $1"
    qvm-run "$1" "$2"
}

# $1 = vmname
function has-gnome-term()
{
    out=$(qvm-run --pass-io "$1" "which gnome-terminal")
    [ -z "$out" ] &&
	return 1
    return 0
}

# $1 = vmname
function qvm-run-term()
{
    if has-gnome-term "$1"
    then
	cmd="gnome-terminal"
    else
	cmd="xfce4-terminal"
    fi

    run-cmd-notify "$1" "$cmd"
}

# $1 = netvm name
# $2 = qvm-ls output
function _ls_using_net()
{
    for line in $2
    do
	local vm
	vm=$(_get_raw_field "$line" 1)

	local netvm
	netvm=$(_get_raw_field "$line" 2)

	if [ "$netvm" = "$1" ]
	then
	    _ls_using_net "$vm" "$2"
	    echo "$vm"
	fi
    done
}

# function qvm-ls-using-net()
# List all running vms that use the given vm as a NETVM
# $1 = netvm
function qvm-ls-using-net()
{
    [ $# -lt 1 ] &&
    	notify "ERR: Missing argument: netvm" &&
    	exit 1

    res=$(qvm-ls --running --raw-data --fields=NAME,NETVM)
    _ls_using_net "$1" "$res"
}

# $1 = netvm
function qvm-shutdown-netvm()
{
    dependent_vms=$(qvm-ls-using-net "$1")

    
    echo ""
    echo "    /!\\ Warning!!! /!\\"
    echo "This will shut down the following vms:"
    echo ""
    echo "$dependent_vms"
    echo "$1"
    echo ""
    read -p "Do you wish to continue? [Y/n]: " ans

    case "$ans" in

	y|Y|yes|Yes|YES);;
	"");;
	
	n|N|no|NO|No|nO)
	    notify "Aborted."
	    return 1
	    ;;

    esac

    for vm in $dependent_vms $1
    do
	qvm-shutdown --wait "$vm" ||
	    return 1
    done
}

# $1 = target vm
# $2 = additional shutdown options
function qvm-restart()
{
    [ $# = 0 ] &&
	{
	    printf "\n%s\n\n" "USAGE: qvm-restart <targetvm> [additional shutdown options]"
	    return 1
	}

    qvm-shutdown --wait $2 $1 &&
	qvm-start $1
}

# $1 = target vm
function qvm-restart-netvm()
{
    [ ! $# = 1 ] &&
	{
	    printf "\n%s\n\n" "USAGE: qvm-restart-vm <targetvm>"
	    return 1
	}

    qvm-restart "$1" "--force"
}

function qvm-active-window-id()
{
    wm_get_active_window_id
}

function qvm-active-window-vmname()
{
    wm_get_active_window_vmname
}

function qvm-active-window-process()
{
    wm_get_active_window_process
}

# $1 = vm name
function qvm-gui-connect()
{
    domain="$(xl domid $1)"
    qubes-guid -d "$domain" -N $1 -Q
}

# $1 = target vm
# $2 = device frontend
# $3 = mount point name
# $4 = mount volume name
# $5 = luks passphrase
qvm-cryptsetup-mount-on-vm()
{
    mntpoint="/mnt/$3"
    cryptoname="/dev/mapper/$3"
    cvolume="/dev/mapper/$4"

    qvm-run --quiet "$1" "mount | grep -q $mntpoint" &&
	return 0;

    qvm-run --quiet "$1" "ls $cryptoname" ||
	{
	    
	    qvm-run --pass-io $1 \
		    "echo '$5' | sudo cryptsetup open /dev/$2 $3" ||
		{
		    echo "FAILED Opening disk with cryptsetup!"
		    return 1
		}
	}

    qvm-run --pass-io $1 \
	    "sudo mkdir -p $mntpoint" ||
	{
	    echo "FAILED to create $mntpoint on $1"
	    return 2
	}

    qvm-run --pass-io $1 \
	    "sudo mount $cvolume $mntpoint" ||
	{
	    echo "FAILED to mount volume $4 on $mntpoint"
	    return 3
	}

    echo "Volume \"$4\" mounted on $1:$mntpoint"
}

# $1 = vm name
qvm-ensure-started()
{
    qvm-is-running "$1" &&
	return 0

    qvm-start "$1" ||
    {
	echo "FAILED to start $1"
	return 1
    }
}
