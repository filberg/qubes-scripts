#!/bin/bash

# This file is part of filberg's qubes-bashed-potatoes
# 
#  Copyright (c) 2020 Filippo Bergamo <fil@filberg.eu>
# 
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software Foundation,
# Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301  USA

srcdir="$(dirname $0)"

. "$srcdir/lib.sh"
. "$srcdir/lib-qvm.sh"


profiles_dir="/etc/qubes/backup"

_env_vars="\
FILBERG_BKP_HOSTVM \
FILBERG_BKP_MOUNTSCRIPT \
FILBERG_BKP_TARGET_DIR"


# $1 = profile name
backup_profile_exists()
{
    [ -f "$profiles_dir/$1.conf" ] &&
	return 0

    return 1
}

# $1 = profile
backup_target_dir_ensure()
{
    qvm-run "$FILBERG_BKP_HOSTVM" "mkdir -p $FILBERG_BKP_TARGET_DIR/$1"
}

# $1 = profile name
# [ $2 ] = "--yes" (to pass --yes option to qvm-backup)
backup_run()
{
    profile="$1"

    backup_profile_exists "$profile" ||
	{
	    printerr "ERROR: profile \"$profile\" does not exist!"
	    return 1
	}
    
    # 1) run host vm
    qvm-is-running "$FILBERG_BKP_HOSTVM" ||
	qvm-start $FILBERG_BKP_HOSTVM ||
	{
	    printerr "FAILED to start $FILBERG_BKP_HOSTVM"
	    return 2
	}

    # 2) mount ssd
    $FILBERG_BKP_MOUNTSCRIPT "$FILBERG_BKP_HOSTVM" mount ||
	return $?

    backup_target_dir_ensure "$profile"

    opts=
    [ "$2" = "--yes" ] &&
	opts="$opts --yes"
    
    qvm-backup $opts --profile "$profile"
}

backup_check_env()
{
    check_env_vars $_env_vars
}
